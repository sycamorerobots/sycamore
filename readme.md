![8c3f45dbff51acf7f27ca5b68131f1e2ebc0c595.png](https://bitbucket.org/repo/Kxdeb4/images/4241210723-8c3f45dbff51acf7f27ca5b68131f1e2ebc0c595.png)


Sycamore is a 2D-3D simulation environment for autonomous mobile robots algorithms. With Sycamore it is possible to test an algorithm as well as to check its behavior under several different situations. Sycamore is a completely modular system, where you can write your own plugin and integrated in an easy way. Sycamore is completely open source and with the available SDK it is possible for everybody to write its own plugin and check with it the behavior of the system.

A full documentation about Sycamore, as well as a FAQ page will be available soon. If you have questions, you can ask the project owners.

**Note: On Mac OS X Systems, Sycamore does not work with Java 7. This is a problem of the LWJGL library, that is necessary to Sycamore to work. Hopefully an update will be released, but until that moment it is necessary to use Sycamore with Java 6 on Mac OS X. When trying to start it with Java 7, it will crash. This does not apply to Windows and Linux systems.**

# 12/08/2014 - New Version Released!
A new version of the source code has been committed. In this version the engine has been updated to work in synch with VirCA, a 3D Internet based interactive virtual environment for collaborative manipulation of robots and other hardware or software equipment.
Note that this ability is currently supported only for Windows.

# 02/12/2013 - New Build Released!
 #A new build of Sycamore (ver 20131202) is available. You can download it from the new Downloads page. This build solves a number of bugs and presents new features:
Tons of minor bugs fixed
More plugins available
Visualizer implemented
Added support for Lights with intensity
Visualizer implemented
Following with directional visibility problem has been studied and the plugins are available
Save and Load projects features implemented
More bugs fixed. Please check the Known bugs wiki page for details.

# 15/09/2013 - New Agreements
The Agreements kit has been updated. The plugins are now the following:
Total agreement - Local coordinates system is completely equal for all the robots. The origin of axes, orientation, directions and measure unit are exactly the same.
Consistent compass - Each robot has its own origin and measure unit, but the cardinal points north, south, east and west (also up and down in 3D) are the same for all the robots.
One axis - Each robot has its own origin and measure unit and just 2 of the cardinal points north, south, east and west (also up and down in 3D) are equal. The others can be different from one robot to another.
Any axis (2D) - Each robot has its own origin and measure unit and just 2 of the cardinal points north, south, east and west are equal. The others can be different from one robot to another. In addition, the mapping of the axes may vary from one robot to another, so that what is X for a robot can correspond to Y for another robot.
Two axes (3D) - Each robot has its own origin and measure unit and just 4 of the cardinal points north and south, east and west, up and down are equal. The others can be different from one robot to another.
Disorientation - Basically the local coordinates system of a robot can be completely different from the one of another robot.

# 05/09/2013 - New Build Released!
A new build of Sycamore (ver 20130905) is available. You can download it from the usual locations. This build solves a number of bugs and presents new features:
The Agreement plugins are fully supported, now the plugins support is complete. Together with the application, 4 new plugins has been distributed, in order to give the ability of having total agreement and no agreement. The old agreement plugins are not working anymore, it is highly suggested not to use them.
Added the support for the visualization of the local coordinate systems
Added a global preferences pane
Added a new About panel that gives informations about the application, the system and the memory usage.
Added a brand new Splash Screen and a better icon
Enhanced the behavior of the menu bars. Now The OS X and Windows menus are different, and the majority of the menu items produce the correct behavior when clicked.
Added the ability of importing/exporting plugins via menu bar
Fixed a bug of the AsynchronousSchedulerPriorityQueue plugin that was generating NullPointerExceptions
Fixed a bug of the slider for animation control that was generating NullPointerExceptions. Now it is possible to use that slider just when the animation is paused and not when is played neither stopped. This is because the slider works just if the timelines of the animation are alive, and when pressing the stop button they are destroyed. This will not apply to the coming soon "Visualizer" application, where it will be always possible to use the animation control slider.

More bugs fixed. Please check the Known bugs wiki page for details.

*Part of this work has been developed during the Thesis of Valerio Volpi, under the supervision of G. Prencipe (University of Pisa, Italy).*